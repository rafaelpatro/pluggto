<?php

/**
 *
 * NOTICE OF LICENSE
 *
 * Todos direitos reservados para Thirdlevel | ThirdLevel All Rights Reserved
 *
 * @company   	ThirdLevel
 * @package    	MercadoLivre
 * @author      André Fuhrman (andrefuhrman@gmail.com)
 * @copyright  	Copyright (c) ThirdLevel [http://www.thirdlevel.com.br]
 *
 */


class Thirdlevel_Pluggto_Block_Cart_Formadepago extends Mage_Payment_Block_Form
{
    protected function _construct()
    {
        parent::_construct();
    }
}

?>